#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "scanner.hpp"
#include "ast.hpp"
#include "parser.hpp"
#include "interpreter.hpp"
#include "error_handler.hpp"

namespace sap
{

    static void run(const std::string &source, Interpreter &interpreter)
    {
        ErrorHandler errors;

        Scanner scanner(source, errors);
        TokenList tokens = scanner.scanTokens();

        Parser parser(std::move(tokens), errors);
        StmtList stmts = parser.parse();

        interpreter.interpret(stmts);
    }

    static void runFile(const std::string &path)
    {
        std::ifstream file(path);
        std::ostringstream ostr;
        ostr << file.rdbuf();
        file.close();

        Interpreter interpreter;
        run(ostr.str(), interpreter);
    }

    static void runPrompt()
    {
        Interpreter interpreter;
        while (true)
        {
            std::cout << ">";

            std::string line;
            getline(std::cin, line);
            run(line, interpreter);
        }
    }
} // namespace sap

int main(int argc, const char **argv)
{
    if (argc > 2)
        std::cerr << "Usage : sap [filename]" << std::endl;
    else if (argc == 2)
    {
        sap::runFile(argv[1]);
    }
    else
    {
        sap::runPrompt();
    }
    return 0;
}